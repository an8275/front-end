function _(e) {
  return document.querySelector(e);
}

document.querySelector(".dropup-btn").addEventListener("click", function () {
  document.querySelector(".dropup-link").classList.toggle("show");
  document.querySelector(".dropup-link").classList.toggle("hide");
});

(function () {
  let activeTabs = document.querySelectorAll(".dropup-link > a");

  activeTabs.forEach((item) => {
    item.addEventListener("click", (e) => {
      _(".dropup-link").classList.remove("show");
      _(".dropup-link").classList.add("hide");
    });
  });
})();

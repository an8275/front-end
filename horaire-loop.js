window.onload = () => {
  let weekDay = [
    "monday",
    "tuesday",
    "wednesday",
    "thursday",
    "friday",
    "saturday",
    "sunday",
  ];

  // WeekDay Loop Data
  let openDays = document.getElementById("openDays");
  let closeDays = document.getElementById("closeDays");

  //Pick Hours Function
  weekDay.forEach(function (day) {
    let divE = document.createElement("div");

    let rootElem = document.createElement("label");
    rootElem.innerHTML = day;
    let select = document.createElement("select");
    for (let i = 0; i < 24; i++) {
      select.innerHTML += `<option value="${i}">${i}h</option>`;
    }
    divE.appendChild(rootElem);
    divE.appendChild(select);
    openDays.appendChild(divE);
  });

  weekDay.forEach(function (day) {
    let divE = document.createElement("div");

    let rootElem = document.createElement("label");
    rootElem.innerHTML = day;
    let select = document.createElement("select");
    for (let i = 0; i < 24; i++) {
      select.innerHTML += `<option value="${i}">${i}h</option>`;
    }
    divE.appendChild(rootElem);
    divE.appendChild(select);
    closeDays.appendChild(divE);
  });
};

window.onload = () => {
  let weekDay = [
    "monday",
    "tuesday",
    "wednesday",
    "thursday",
    "friday",
    "saturday",
    "sunday",
  ];

  // WeekDay Loop Data
  let openDays = document.getElementById("openDaysShow");
  let closeDays = document.getElementById("closeDaysShow");

  //Pick Hours Function
  weekDay.forEach(function (day) {
    let divE = document.createElement("div");

    let days = document.createElement("span");
    days.innerHTML = day;
    days.classList.add("days");

    let hours = document.createElement("span");
    hours.innerHTML = ":  h00";
    hours.classList.add("hours");

    divE.appendChild(days);
    divE.appendChild(hours);
    openDays.appendChild(divE);
  });

  weekDay.forEach(function (day) {
    let divEC = document.createElement("div");

    let days = document.createElement("span");
    days.innerHTML = day;
    days.classList.add("days");

    let hours = document.createElement("span");
    hours.innerHTML = ": h00";
    hours.classList.add("hours");

    divEC.appendChild(days);
    divEC.appendChild(hours);
    closeDays.appendChild(divEC);
  });
};
